% p5
% Andrew Lampert
% 250 643 797
% ALamper@uwo.ca

function w = p5( X_train, Y_train, iterNum )

	% Getting size of X_train
	[n_row, n_col] = size(X_train);

	best_w = randn(n_row + 1, 1);
	best_err = 1000;

	for iter_i = 1 : iterNum

		test_w = randn(n_row+1, 1);

		C = p4(test_w, X_train);

		[test_err, conf] = p2(C, Y_train);

		if	test_err	< best_err
			best_err	= test_err;
			best_w		= test_w;
		end

		% for element_i = 1 : n_row * n_col

		% 	% Initializing test weights
		% 	test_w = best_w;

		% 	% Changing 1 test weight val to be a new random val
		% 	test_w(element_i) = randn;

		% 	% Calculating classification based on test weights
		% 	C = p4(test_w, X_train);

		% 	% Calculating error
		% 	[test_err, conf] = p2(C, Y_train);

		% 	% If this round has the best error rate, save the error rate and weights
		% 	if	test_err	< best_err
		% 		best_err	= test_err;
		% 		best_w		= test_w;
		% 	end

		% end

	end

	w = best_w;

end