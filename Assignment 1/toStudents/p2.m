% p2
% Andrew Lampert
% 250 643 797
% ALamper@uwo.ca

% C : Classification Results
% T : True values

% Returns error rate and confusion matrix.

function [err,CONF] = p2(C,T)

	% err = Incorrect count / total count

	% Conf(i, j) : m x m matrix where (i, j) is the number of examples of class
	% i that are classified as j.

	% Finding all unique elements to build CONF with
	unique_elements = unique([C;T]);

	% Creating array of zeros
	CONF = zeros(length(unique_elements));

	% Try to replace later
	
	for index = 1 : length(C)
		CONF(T(index), C(index)) = CONF(T(index), C(index)) + 1; 
	end


	% Calculating error rate
	wrong_count = length(C) - sum(T == C);
	err = wrong_count / length(C);

end

