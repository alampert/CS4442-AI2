% p9
% Andrew Lampert
% 250 643 797
% ALamper@uwo.ca

% p9 calculates the weight matrix using softmax single sample rule

function W = p9(X_Train, Y_Train, iterNum, WInit, alpha )

	% w = Weight
	W = WInit;

	% Adding Bias
	X = [ones(length(X_Train), 1), X_Train];

	for iter_i = 1 : iterNum

		% Calculating softmax

		for sum_i = 1 : length(W)

			% Isolating x^i
			x_i = X(sum_i, :);

			% Isolating y^i
			y_i = zeros(length(W), 1);
			y_i(sum_i) = 1;

			% Updating wx
			for wx_i = 1 : length(W)
				wx(wx_i, 1) =  W(wx_i, :) * transpose(x_i);
			end

			W = W + alpha * (y_i - softmax_calc(wx)) * x_i;

		end
	end
end
