% p4
% Andrew Lampert
% 250 643 797
% ALamper@uwo.ca

% Determines classification, C, of entries from X given w weights

function C = p4(w, X)

	[x_rows, x_cols] = size(X);

	weighted_sample = 0;

	for row_i = 1 : x_rows

		weighted_sample = w(1);

		for col_i = 1 : x_cols

			weighted_sample = weighted_sample + w(col_i + 1) * X(row_i, col_i);

		end

		if weighted_sample > 0
			new_val = 1;
		elseif weighted_sample < 0
			new_val = 2;
		else 
			disp(['weighted_sample is equal to: ', weighted_sample])	
		end

		C(row_i, 1) = new_val;

		weighted_sample = 0;

	end

end